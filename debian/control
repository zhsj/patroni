Source: patroni
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders: Michael Banck <michael.banck@credativ.de>,
 Adrian Vondendriesch <adrian.vondendriesch@credativ.de>,
Build-Depends:
 debhelper (>= 10),
 dh-python,
 libjs-jquery,
 libjs-mathjax,
 libjs-underscore,
 pyflakes3,
 python3,
 python3-boto3,
 python3-cdiff,
 python3-click (>= 4.1),
 python3-consul (>= 0.7.0),
 python3-dateutil,
 python3-dnspython,
 python3-doc,
 python3-etcd (>= 0.4.3),
 python3-flake8,
 python3-kazoo,
 python3-mccabe,
 python3-mock,
 python3-kubernetes,
 python3-prettytable (>= 0.7),
 python3-psutil,
 python3-psycopg2 (>= 2.6.1),
 python3-pycodestyle,
 python3-pytest,
 python3-pytest-cov,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 python3-tz,
 python3-tzlocal,
 python3-yaml,
 sphinx-common
Standards-Version: 4.1.5
X-Python3-Version: >= 3.5
Vcs-Browser: https://salsa.debian.org/postgresql/patroni
Vcs-Git: https://salsa.debian.org/postgresql/patroni.git
Homepage: https://github.com/zalando/patroni

Package: patroni
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, lsb-base (>= 3.0-6), python3-psycopg2,
 python3-etcd (>= 0.4.3) | python3-consul (>= 0.7.0) | python3-kazoo |
 python3-kubernetes | python3-pysyncobj, python3-cdiff
Recommends: iproute2
Suggests: postgresql, etcd-server | consul | zookeeperd, vip-manager, haproxy, patroni-doc
Description: PostgreSQL High Availability with ZooKeeper, etcd, Consul, or Kubernetes
 Patroni is a template for a customized, high-availability PostgreSQL solution
 using Python and a distributed configuration store (ZooKeeper, etcd or
 Consul), or Kubernetes. It provides a REST API on each Postgres node of the
 cluster allowing for remote management and monitoring.

Package: patroni-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Suggests: postgresql, etcd-server | consul | zookeeperd, haproxy
Description: PostgreSQL High Availability (documentation)
 Patroni is a template for a customized, high-availability PostgreSQL solution
 using Python and a distributed configuration store (ZooKeeper, etcd or
 Consul), or Kubernetes. It provides a REST API on each Postgres node of the
 cluster allowing for remote management and monitoring.
 .
 This package provides the documentation.
